package Modelo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author daniel
 */
public class CategoriasModelo {
     int codigoCategoria = 0;
     String descripcion = "";

    public CategoriasModelo() {
        
    }

    public int getCodigoCategoria() {
        return codigoCategoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setCodigoCategoria(int codigoCategoria) {
        this.codigoCategoria = codigoCategoria;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
   
}
