/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author daniel
 */
public class ProductosModelo {
    int codigoproducto = 0;
    String descripcion = "";
    int existencia = 0;
    double precioUnitario = 0.00;
    double precioPorDocena = 0.00;
    double precioPorMayor = 0.00;
    int Categorias_codigoCategoria = 0;
    int Marcas_codigoMarca = 0;
    int Tallas_codigoTalla= 0;
    
    void ProductosModelo(){
        
    }

    public int getCategorias_codigoCategoria() {
        return Categorias_codigoCategoria;
    }

    public int getCodigoproducto() {
        return codigoproducto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getExistencia() {
        return existencia;
    }

    public int getMarcas_codigoMarca() {
        return Marcas_codigoMarca;
    }

    public double getPrecioPorDocena() {
        return precioPorDocena;
    }

    public double getPrecioPorMayor() {
        return precioPorMayor;
    }

    public double getPrecioUnitario() {
        return precioUnitario;
    }

    public int getTallas_codigoTalla() {
        return Tallas_codigoTalla;
    }

    public void setCategorias_codigoCategoria(int Categorias_codigoCategoria) {
        this.Categorias_codigoCategoria = Categorias_codigoCategoria;
    }

    public void setCodigoproducto(int codigoproducto) {
        this.codigoproducto = codigoproducto;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setExistencia(int existencia) {
        this.existencia = existencia;
    }

    public void setMarcas_codigoMarca(int Marcas_codigoMarca) {
        this.Marcas_codigoMarca = Marcas_codigoMarca;
    }

    public void setPrecioPorDocena(double precioPorDocena) {
        this.precioPorDocena = precioPorDocena;
    }

    public void setPrecioPorMayor(double precioPorMayor) {
        this.precioPorMayor = precioPorMayor;
    }

    public void setPrecioUnitario(double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public void setTallas_codigoTalla(int Tallas_codigoTalla) {
        this.Tallas_codigoTalla = Tallas_codigoTalla;
    }    
}
