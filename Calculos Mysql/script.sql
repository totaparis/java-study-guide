CREATE DATABASE IF NOT EXISTS db_empresa_ventas;
USE db_empresa_ventas;

DROP TABLE Categorias;
CREATE TABLE Categorias(
 codigoCategoria INT NOT NULL, 
 descripcion VARCHAR(150),
 PRIMARY KEY (codigoCategoria)
);

DROP TABLE Marcas;
CREATE TABLE Marcas(
 codigoMarca INT NOT NULL, 
 descripcion VARCHAR(45),
 PRIMARY KEY(codigoMarca)
);

DROP TABLE Tallas;
CREATE TABLE Tallas(
codigoTalla INT NOT NULL, 
descripcion VARCHAR(45),
PRIMARY KEY (codigoTalla)
);

DROP TABLE Productos;
CREATE TABLE Productos(
codigoProducto INT, 
descripcion VARCHAR(100),
existencia INT(100),
precioUnitario DECIMAL (10,2),
precioPorDocena DECIMAL(10,2),
precioPorMayor DECIMAL(10,2),
Categorias_codigoCategoria INT,
Marcas_codigoMarca INT,
Tallas_codigoTalla INT,
PRIMARY KEY (codigoProducto),
FOREIGN KEY (Categorias_codigoCategoria) REFERENCES Categorias(codigoCategoria),
FOREIGN KEY (Marcas_codigoMarca) REFERENCES Marcas(codigoMarca),
FOREIGN KEY (Tallas_codigoTalla) REFERENCES Tallas(codigoTalla)
);


#INSERTAR 10 CATEGORIAS
INSERT INTO Categorias (codigoCategoria,descripcion) VALUES ('1','Ropa de Hombre');
INSERT INTO Categorias (codigoCategoria,descripcion) VALUES ('2','Ropa de Mujer');
INSERT INTO Categorias (codigoCategoria,descripcion) VALUES ('3','Ropa de Niño');
INSERT INTO Categorias (codigoCategoria,descripcion) VALUES ('4','Ropa de Niña');
INSERT INTO Categorias (codigoCategoria,descripcion) VALUES ('5','Ropa de Bebe');
INSERT INTO Categorias (codigoCategoria,descripcion) VALUES ('6','Zapatos Formales');
INSERT INTO Categorias (codigoCategoria,descripcion) VALUES ('7','Zapatos Casuales');
INSERT INTO Categorias (codigoCategoria,descripcion) VALUES ('8','Zapatos Deportivos');
INSERT INTO Categorias (codigoCategoria,descripcion) VALUES ('9','Perfumes de Hombre');
INSERT INTO Categorias (codigoCategoria,descripcion) VALUES ('10','Perfumes de Mujer');

#INSERTAR 10 MARCAS
INSERT INTO Marcas (codigoMarca,descripcion) VALUES ('1','Adidas');
INSERT INTO Marcas (codigoMarca,descripcion) VALUES ('2','Converse');
INSERT INTO Marcas (codigoMarca,descripcion) VALUES ('3','Columbia');
INSERT INTO Marcas (codigoMarca,descripcion) VALUES ('4','Puma');
INSERT INTO Marcas (codigoMarca,descripcion) VALUES ('5','Nike');
INSERT INTO Marcas (codigoMarca,descripcion) VALUES ('6','Vans');
INSERT INTO Marcas (codigoMarca,descripcion) VALUES ('7','Gucci');
INSERT INTO Marcas (codigoMarca,descripcion) VALUES ('8','LaCoste');
INSERT INTO Marcas (codigoMarca,descripcion) VALUES ('9','Versace');
INSERT INTO Marcas (codigoMarca,descripcion) VALUES ('10','Prada');

# INSERTAR 10 TALLAS
INSERT INTO Tallas (codigoTalla,descripcion) VALUES ('1','XXS');
INSERT INTO Tallas (codigoTalla,descripcion) VALUES ('2','XS');
INSERT INTO Tallas (codigoTalla,descripcion) VALUES ('3','S');
INSERT INTO Tallas (codigoTalla,descripcion) VALUES ('4','M');
INSERT INTO Tallas (codigoTalla,descripcion) VALUES ('5','L');
INSERT INTO Tallas (codigoTalla,descripcion) VALUES ('6','XL');
INSERT INTO Tallas (codigoTalla,descripcion) VALUES ('7','XXL');
INSERT INTO Tallas (codigoTalla,descripcion) VALUES ('8','USA 10.5');
INSERT INTO Tallas (codigoTalla,descripcion) VALUES ('9','USA 12');
INSERT INTO Tallas (codigoTalla,descripcion) VALUES ('10','USA 13');


#EJERCICIO 1: PROCEDIMIENTO ALMACENADO COUNT MARCAS,TALLAS, CATEGORIAS 

#MARCAS
DROP PROCEDURE IF EXISTS count_marcas;
DELIMITER //
CREATE PROCEDURE count_marcas()
BEGIN
SELECT COUNT(Marcas.codigoMarca) AS total_marcas  FROM Marcas;
END
// 
DELIMITER ;
CALL count_marcas;

#TALLAS 
DROP PROCEDURE IF EXISTS count_tallas;
DELIMITER //
CREATE PROCEDURE count_tallas()
BEGIN
SELECT COUNT(Tallas.codigoTalla) AS total_tallas  FROM tallas;
END
// 
DELIMITER ;
CALL count_tallas;

#CATEGORIAS 
DROP PROCEDURE IF EXISTS count_categorias;
DELIMITER //
CREATE PROCEDURE count_categorias()
BEGIN
SELECT COUNT(Categorias.codigoCategoria) AS total_categorias  FROM categorias;
END
// 
DELIMITER ;
CALL count_categorias;



#EJERCICIO 2 : Ingresar un registro y calcular sus ganancias
INSERT INTO Productos (codigoProducto,descripcion,existencia,precioUnitario,precioPorDocena,precioPorMayor,Categorias_codigoCategoria,Marcas_codigoMarca,Tallas_codigoTalla)
VALUES ('1', 'Camiseta Umbro', '10', '189', '175.50', '162', '1', '1', '4');
#FORMULA: PRECIO DE COSTO + (PRECIO DE COSTO * PORCENTAJE DE GANANCIA)
#PRECIO DE GANANCIA 40%  = 135 + (135 * 40%)
#PRECIO POR DOCENA 30%  = 135 + (135 * 30%)
#PRECIO POR MAYOR 20% = 135 + (135 * 20%)

DROP PROCEDURE IF EXISTS calculo_precio;
DELIMITER //
CREATE PROCEDURE calculo_precio(
    IN precio_costo DECIMAL (10,2)
)
BEGIN
DECLARE precio_unitario DECIMAL (10,2) DEFAULT 0;
DECLARE precio_docena DECIMAL (10,2) DEFAULT 0;
DECLARE precio_mayor DECIMAL (10,2) DEFAULT 0;

SET @precio_unitario = precio_costo + (precio_costo * 0.40);
SET @precio_docena = precio_costo + (precio_costo * 0.30);
SET @precio_mayor = precio_costo + (precio_costo * 0.20);

SELECT precio_costo, @precio_unitario as precio_unitario, @precio_docena as precio_docena, @precio_mayor as precio_mayor ;

END
// 
DELIMITER ;
CALL calculo_precio(135);
